<?php

namespace Arharist\Layout\Commands;

use Arharist\Layout\Commands\DirectoryManagement;
use Illuminate\Console\Command;

class IntegrateLayout extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'arharist.workin.on:layout';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Integrate a theme layout into your project';

    protected $templatePath = 'vendor/arharist/layout/src/templates/';

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle()
    {
        if (! file_exists($this->templatePath)) {
            $this->templatePath = 'packages/arharist/layout/src/templates/';
        } 

        $this->copyView();
        $this->updateRoutes();
        $this->publishHomeController();
    }

    private function copyView()
    {
        $viewsPath = base_path('resources/views/');

        $this->createDirectories($viewsPath);

        $files = $this->getViews();

        foreach ($files as $file => $blade) {
            $sourceFile = base_path($this->templatePath.'theme/').$file.'.stub';
            $destinationFile = $viewsPath.$blade;
            $this->publishFile($sourceFile, $destinationFile, $blade);
        }

        $this->publishFile(base_path($this->templatePath.'theme/layouts/logo.stub'), public_path('logo.jpg'), base_path($this->templatePath.'theme/layouts/logo.jpg'));
    }

    private function createDirectories($viewsPath)
    {
        DirectoryManagement::createDirectoryIfNotExist($viewsPath.'layouts');
        DirectoryManagement::createDirectoryIfNotExist($viewsPath.'auth');

        DirectoryManagement::createDirectoryIfNotExist($viewsPath.'auth/passwords');
        DirectoryManagement::createDirectoryIfNotExist($viewsPath.'auth/emails');
    }

    private function getViews()
    {
        return [
            'layouts/app'               => 'layouts/app.blade.php',
            'layouts/sidebar'           => 'layouts/sidebar.blade.php',
            'layouts/datatables_css'    => 'layouts/datatables_css.blade.php',
            'layouts/datatables_js'     => 'layouts/datatables_js.blade.php',
            'layouts/menu'              => 'layouts/menu.blade.php',
            'layouts/home'              => 'home.blade.php',
            'auth/login'                => 'auth/login.blade.php',
            'auth/register'             => 'auth/register.blade.php',
            'auth/email'                => 'auth/passwords/email.blade.php',
            'auth/reset'                => 'auth/passwords/reset.blade.php',
            'emails/password'           => 'auth/emails/password.blade.php',
        ];
    }

    private function publishFile($sourceFile, $destinationFile, $fileName)
    {
        if (file_exists($destinationFile) && !$this->confirmOverwrite($destinationFile)) {
            return;
        }

        copy($sourceFile, $destinationFile);

        $this->comment($fileName.' published');
        $this->info($destinationFile);
    }

    private function updateRoutes()
    {
        $path = app_path('Http/routes.php');

        $prompt = 'Existing routes.php file detected. Should we add standard routes? (y|n) :';
        if (file_exists($path) && !$this->confirmOverwrite($path, $prompt)) {
            return;
        }

        $routeContents = file_get_contents($path);

        $routesTemplate = file_get_contents(base_path($this->templatePath.'component/Route/').'auth.stub');

        $routeContents .= "\n\n".$routesTemplate;

        file_put_contents($path, $routeContents);
        $this->comment("\nAdd Route Success");
    }

    private function publishHomeController()
    {
        $templateData = file_get_contents(base_path($this->templatePath.'component/Controller/').'home_controller.stub');

        $templateData = $this->fillTemplate($templateData);

        $controllerPath = app_path('Http/Controllers/');

        $fileName = 'HomeController.php';

        if (file_exists($controllerPath.$fileName) && !$this->confirmOverwrite($fileName)) {
            return;
        }

        DirectoryManagement::createFile($controllerPath, $fileName, $templateData);

        $this->info('HomeController created');
    }

    /**
     * Replaces dynamic variables of template.
     *
     * @param string $templateData
     *
     * @return string
     */
    private function fillTemplate($templateData)
    {
        $templateData = str_replace('$NAMESPACE_CONTROLLER$', 'App\Http\Controllers', $templateData );

        $templateData = str_replace('$NAMESPACE_REQUEST$', 'App\Http\Requests', $templateData );

        return $templateData;
    }

    /**
     * @param $fileName
     * @param string $prompt
     *
     * @return bool
     */
    protected function confirmOverwrite($fileName, $prompt = '')
    {
        $prompt = (empty($prompt))
            ? $fileName.' already exists. Do you want to overwrite it? [y|n]'
            : $prompt;

        return $this->confirm($prompt, false);
    }
}
